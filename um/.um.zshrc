# um
# https://news.ycombinator.com/item?id=17797355

# export UMPATH="/path/to/man/folder"

function mdless() {
      pandoc -s -f markdown -t man $1 | groff -T utf8 -man | less
}
umedit() { vim $UMPATH/"$1".html.md; }
um() { mdless $UMPATH/"$1".html.md; }
umls() { ls $UMPATH }
