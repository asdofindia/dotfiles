# fzf completions
source /usr/share/skim/key-bindings.zsh
source /usr/share/skim/completion.zsh

# From https://remysharp.com/2018/08/23/cli-improved#fzf--ctrlr
alias preview="fzf --preview 'bat --color \"always\" {}'"
# add support for ctrl+o to open selected file in vim
export FZF_DEFAULT_OPTS="--bind='ctrl-o:execute(vim {})+abort'"

# https://github.com/junegunn/fzf#respecting-gitignore
export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
