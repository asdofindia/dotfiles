# Dotfiles

These dotfiles are maintained using [stow](https://www.gnu.org/software/stow/manual/stow.html)

**Important Caveat**: The README maybe outdated, incorrect, or incomplete. Please read the source code and see for yourself before doing things.

## Installation

If you are cloning to ~/dotfiles, you can straightaway start using `stow zsh` and other commands.

But if you are cloning to a subdirectory, you would need to do this:

`stow -t ~ zsh`

(This sets the target directory where symlinks are created to `~`)

Therefore it makes sense to add `alias stow='stow -t ~'` to your `.zshrc`

## Features

`stow antigen`

: enables antigen for management of zsh

`stow mkcd`

: enables a function to make directory and change to it at the same time

`stow um`

: enables editing custom man pages with um

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
stow vim
mkdir -p ~/.vim/swap
vim +PlugInstall
```

: enables vim with Vundle plugin manager (Note: You will also need to [compile](https://github.com/ycm-core/YouCompleteMe) YCM to get it working. You will need to install [skim](https://github.com/lotabout/skim) to get that working)

`stow fzf`

:enables various fzf features

`stow compton`

: compton configuration

### Unmaintained features

These were added in the past when I was trying them out. They remain only in the hope that me or someone else may find them useful later.

`stow bspwm`

: bspwm. Make sure to use xinitrc to start bspwm. And enable sxhkd as well. Depends on `sudo pacman -S bspwm`

`stow sxhkd`

: enable sxhkd. Depends on `sudo pacman -S sxhkd`

`stow polybar`

: polybar. Depends on `aur sync polybar && sudo pacman -S polybar`

## Assumptions

```
sudo pacman -S skim picom termite i3-gaps bat z ranger
```
