# Set up the prompt

autoload -Uz promptinit
promptinit
prompt adam1

setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'

[[ -e ~/.zshrc.private ]] && source ~/.zshrc.private

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim' # same :D
fi

## set gopath
export GOPATH="$HOME/.go"

## For gpg-agent https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
GPG_TTY=`tty`
export GPG_TTY

## Ensuring uniquity in path
typeset -U path

## Add ruby local bins to path 
## Source: https://guides.rubygems.org/faqs/
if which ruby >/dev/null && which gem >/dev/null; then
	path=($(ruby -r rubygems -e 'puts Gem.user_dir')/bin $path[@])
fi

## Add other paths which are predictable
path=(~/.local/bin ~/.go/bin ~/.cargo/bin ~/.yarn/bin $path[@])

## Add android tools to path
path=($ANDROID_HOME/emulator $ANDROID_HOME/tools $ANDROID_HOME/tools/bin $ANDROID_HOME/platform-tools $path[@])

## Make ZSH autocomplete dotfiles
setopt globdots
setopt autocd

## Various aliases
alias bj="bundle exec jekyll"
alias rsync="rsync --info=progress2"
alias i3config="$EDITOR ~/.config/i3/config"
alias bspconfig="$EDITOR ~/.config/bspwm/bspwmrc ~/.config/sxhkd/sxhkdrc ~/.config/polybar/config"
alias lg='lazygit'
alias julia='julia --color=yes'
alias zshrc='$EDITOR ~/.zshrc && source ~/.zshrc'
alias stow='stow -t ~'
alias open='xdg-open'

# This alias from https://github.com/beyondgrep/ack2/issues/28#issuecomment-276645667 helps ack ignore gitignored files
alias gack='git ls-files -oc --exclude-standard | ack -x'

# source all zsh things
for file in ~/.*.zshrc(N); do
    source "$file"
done
