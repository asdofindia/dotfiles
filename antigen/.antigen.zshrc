export ZSH_ANTIGEN=/usr/share/zsh/share/antigen.zsh
source $ZSH_ANTIGEN

antigen use oh-my-zsh

antigen bundle archlinux
antigen bundle git
antigen bundle systemd
antigen bundle z
antigen bundle zsh-users/zsh-syntax-highlighting

antigen theme robbyrussell

antigen apply
